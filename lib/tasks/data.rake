namespace :data do
  desc 'Load sample data'
  task :load => :environment do
    Panel.destroy_all
    p = Panel.create!(:name => 'Desdr Panel')
    w = UrlWidget.new(:name => 'Jenkins', :panel => p)
    w.url ='http://ci.serpro/view/DESDR%20Dashboard/'
    w.save
    w = UrlWidget.new(:name => 'Voce.serpro', :panel => p)
    w.url ='http://voce.serpro'
    w.save
#    AskbotWidget.create!(:panel => p)
  end
end
