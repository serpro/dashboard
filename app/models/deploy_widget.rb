class DeployWidget < Widget

  def self.permit
    [:system,:identifier,:who, :name]
  end

  def system
    self.settings(:data).system
  end

  def system= system
    self.settings(:data).system = system
  end

  def identifier
    self.settings(:data).identifier
  end

  def identifier= identifier
    self.settings(:data).identifier = identifier
  end

  def who
    self.settings(:data).who
  end

  def who= who
    self.settings(:data).who = who
  end

end
