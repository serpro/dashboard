class AskbotWidget < Widget

  include HTTParty
  base_uri 'oraculo.serpro/api/v1'

  # Example:
  #
  # {{"tags"=>["expressov3"], "answer_count"=>3, "id"=>146, "last_activity_by"=>{"username"=>"Lindomar Oliveira", "id"=>143}, "view_count"=>25, "last_activity_at"=>"1434035997", "title"=>"Contatos do Expresso no Celular Offline", "url"=>"http://oraculo.serpro/pergunta/146/contatos-do-expresso-no-celular-offline/", "author"=>{"username"=>"Rafael Almeida Fernandez Soto", "id"=>41}, "added_at"=>"1433945509", "summary"=>"<p>Alguem conhece solução para dispositivo móvel IOS ou Android para sincronizar os contatos do expresso e disponibiliza-los de forma offline ? </p>\n", "score"=>0},
# {"tags"=>["expressov3"], "answer_count"=>3, "id"=>146, "last_activity_by"=>{"username"=>"Lindomar Oliveira", "id"=>143}, "view_count"=>25, "last_activity_at"=>"1434035997", "title"=>"Contatos do Expresso no Celular Offline", "url"=>"http://oraculo.serpro/pergunta/146/contatos-do-expresso-no-celular-offline/", "author"=>{"username"=>"Rafael Almeida Fernandez Soto", "id"=>41}, "added_at"=>"1433945509", "summary"=>"<p>Alguem conhece solução para dispositivo móvel IOS ou Android para sincronizar os contatos do expresso e disponibiliza-los de forma offline ? </p>\n", "score"=>0}}
  def questions
    load_data if self.askbot_cache.nil? || self.expired?
    questions = self.askbot_cache || []
    questions  = questions[0..1]
    questions  
  end

  def user(user_id)
    self.load_user(user_id)
  end

  def expired?
    return true if self.updated_at.nil?
    (self.updated_at > (DateTime.now - self.timeout))
  end

  protected

  def timeout
    2.hours
  end

  def load_data
    self.askbot_cache = self.class.get("/questions?sort=relevance-asc")['questions']
  end

  def load_user(user_id)
    self.settings(:data).askbot_cache_user ||= {}
    self.askbot_cache_user[user_id] = self.class.get("/users/#{user_id}")
    self.askbot_cache_user[user_id]
  end

  def askbot_cache
    self.settings(:data).askbot_cache
  end

  def askbot_cache= askbot_cache
    self.settings(:data).askbot_cache = askbot_cache
  end

  def askbot_cache_user
    self.settings(:data).askbot_cache_user
  end

end
