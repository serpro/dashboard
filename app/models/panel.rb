class Panel < ActiveRecord::Base

  #FIXME make this test
  module Visibility
    PRIVATE = 1
    PUBLIC_TO_VIEW = 2
    PUBLIC_TO_EDIT = 3

    def self.all(current_user)
      options = [PUBLIC_TO_EDIT]
      options += [PRIVATE, PUBLIC_TO_VIEW] if current_user
      options
    end
  end

  
  validates :name , presence: true
  has_many :widgets, :dependent => :destroy

  validate :permitted_visibility
 
  scope :active, lambda { where(:active => true) }
  scope :inactive, lambda { where(:active => false) }

  scope :visible_to_me, -> user {
    conditions = []
    conditions.push("users.id = #{user.id}") unless user.nil?
    conditions.push("panels.visibility = #{Visibility::PUBLIC_TO_VIEW}")
    conditions.push("panels.visibility = #{Visibility::PUBLIC_TO_EDIT}")
    joins("LEFT JOIN panels_users ON panels.id = panels_users.panel_id LEFT JOIN users ON users.id = panels_users.user_id").
    where(conditions.join(" OR ")).order(:created_at)
  }

  self.per_page = 5

  #FIXME make this test
  has_and_belongs_to_many :users

  def permitted_visibility
    if self.users.empty? && self.visibility != Visibility::PUBLIC_TO_EDIT
      errors.add(:visibility, I18n.t(:visibity_not_allowed))
    end
  end

  def allow_edit?(user)
     could_edit = self.visibility == Visibility::PUBLIC_TO_EDIT
     self.users.include?(user) unless could_edit
     could_edit
  end

end
