class JenkinsWidget < Widget

  include HTTParty

  after_initialize do
    self.class.base_uri self.class.set_uri
  end

  def self.permit
    super + [:include_systems_re, :job_path]
  end

  def self.set_uri(host= nil)
    data = self.servers_config.detect{|c| (c.blank? ?  false : c['default']) == true} || {}
    host = data.has_key?('host') ? data['host'] : 'define an uri'
    self.base_uri(host)
  end

  def self.job_status(color)
    {
      'red' => 'failing',
      'blue' => 'successful',
      'yellow' => 'unstable',
    }[color] || 'blue'
  end

  def pretty_name(name)
    name.gsub(/#{prefix_expression}/,'')
  end

  def prefix_expression
    'DESDR.DE[0-9|ED|CT]+.'
  end

  # Example of return
  # {"GED Build"=>0.0, "AFD"=>0.0, "ExtraTomDanado"=>20.0}
  def jobs
    load_data #if self.sonar_cache.nil? || self.expired?
    jobs_info = jenkins_cache['jobs']
    jobs_info.select!{|job| job['url'].match(/#{self.include_systems_re}/)}
    jobs_info = jobs_info.map do |job|
      parse_json_job(job)
    end
    jobs_info
  end

  def expired?
    return true if self.updated_at.nil?
    (self.updated_at > (DateTime.now - self.timeout))
  end

  def include_systems_re
    self.settings(:data).include_systems_re
  end

  def include_systems_re= re
    self.settings(:data).include_systems_re = re
  end

  def job_path
    self.settings(:data).job_path
  end

  def job_path= job_path
    self.settings(:data).job_path = job_path
  end

  protected

  def parse_json_job(jenkins_job)
    begin
      time = jenkins_job['lastCompletedBuild']['timestamp']
    rescue
      time = Time.at(0).to_i
      jenkins_job['lastCompletedBuild'] = {}
    end
    time = Time.at(time/1000).to_datetime
    jenkins_job['lastCompletedBuild']['datetime'] = time
    lastHealthReport = jenkins_job['healthReport'].first
    jenkins_job['lastHealthReport'] = lastHealthReport
    jenkins_job['lastHealthReport'] ||= {}
    jenkins_job['lastBuild'] ||= {}
    jenkins_job
  end

  def timeout
    0.minutes
#    2.hours
  end

  def load_data
    job_path = self.job_path.gsub(self.class.base_uri, '')
    data = self.class.get("#{job_path}/api/json?tree=jobs[name,url,color,lastCompletedBuild[timestamp],lastBuild[number,url],healthReport[description,score]]", :basic_auth => auth)
    self.jenkins_cache = data.response.is_a?(Net::HTTPOK) ? data.parsed_response : []
  end

  def auth
    {:username => username, :password => password}
  end

  def jenkins_cache
    self.settings(:data).jenkins_cache
  end

  def jenkins_cache= jenkins_cache
    self.settings(:data).jenkins_cache = jenkins_cache
  end

  def self.servers_config
    widget_config['servers'] || []
  end

  def servers_config
    self.class.servers_config
  end

  def username
    return nil if widget_config.blank?
    widget_config['authentication']['username']
  end

  def password
    return nil if widget_config.blank?
    widget_config['authentication']['password']
  end

end
