class Widget < ActiveRecord::Base

  @@widget_config = {}

  validates_presence_of :name

  #FIXME make this test
  scope :active, lambda { where(:active => true) }

  #FIXME validates this presence
  belongs_to :panel

  has_settings :data

  after_initialize do
    self.load_config
  end

  def self.permit
    [:name, :speed]
  end

  def self.widget_types
    [SonarWidget, SonarSmileWidget, AskbotWidget, UrlWidget, JenkinsWidget]
  end

  def reload_data!
    #Subscribe this method in child class
  end


  #FIXME make this test
  def switch_activation!
    value = self.active ?  false : true
    self.update!(:active => value)
  end

  #FIXME make this test
  def self.load_config
    file_path = File.expand_path("#{Rails.root}/config/widgets/#{self.name.underscore}.yml", __FILE__)
    return nil unless File.exists?(file_path)
    @@widget_config ||= {}
    @@widget_config[self.name.underscore] = YAML.load(ERB.new(File.read(file_path)).result)
  end

  def load_config
    self.class.load_config
  end

  #FIXME make this test
  def self.widget_config
    @@widget_config[self.name.underscore]
  end

  #FIXME make this test
  def widget_config
    self.class.widget_config#[self.class.name.underscore]
  end
end
