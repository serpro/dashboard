class GitlabWidget < Widget

  @@current_host = nil

  include HTTParty

  after_initialize do
    self.labels ||= []
    self.projects ||= []
    self.class.base_uri self.class.set_uri
  end

  def self.servers_config
    widget_config['servers'] || []
  end

  def servers_config
    self.class.servers_config
  end

  def self.set_uri(host= nil)
    @@current_host = host || (self.servers_config.detect{|c| c['default'] == true}['host'])
    self.base_uri(@@current_host + "/api/v3")
  end

  def current_host
    @@current_host
  end

  def reload_data!
    self.control_config[:issues_updated_at] = nil
    self.labels = []
    self.issues = []
    self.load_issues
  end

  def self.server_name(host)
#raise servers_config.inspect + '   ' + host.inspect
    data = servers_config.detect{|s| (s.blank? ? false : s['host'] == host) } || {}
    data['name']
  end

  def self.permit
    [:name, activated_projects: []]
  end

  def private_token
    servers_config.detect{|c| self.class.base_uri.match(c['host'])}['private_token']
  end

  def limit
    30
  end

  def timeout
    2.hours
  end

  def activated_projects= projects
    for i in 0...self.projects.length
      self.projects[i][:active] = true if projects.include?(self.projects[i][:id].to_s)
    end
    self.save
  end

  def activated_projects
    self.projects.select{|p| p[:active]}
  end

  def load_my_projects
    return self.projects unless self.expired?('projects')
    self.servers_config.map do |server|
      self.class.set_uri(server['host'])
      self.class.get("/projects/owned?private_token=#{self.private_token}&per_page=#{self.per_page}", :verify => false).map do |project|
        self.projects.push(parse_project_attributes(project))
      end
      self.control_config[:projects_updated_at] = DateTime.now
      self.save
    end
    self.projects
  end

  def load_issues
    load_my_projects if self.expired?('projects')
    return self.issues  unless self.expired?('issues')
    self.issues = []
    self.activated_projects.map do |project|
      url = "/projects/#{project['id']}/issues?private_token=#{self.private_token}"
      
      page = 1
      begin
        self.class.set_uri(project[:host])
        data = self.class.get("/projects/#{project[:id]}/issues?private_token=#{self.private_token}&per_page=#{self.per_page}&page=#{page}", :verify => false)
        data.map do |issue|
          issue = parse_issue_attributes(issue)
          self.labels = (self.labels + issue[:labels]).uniq
          self.issues.push(issue)
        end
        page += 1
      end while !data.empty?
    end
    self.control_config[:issues_updated_at] = DateTime.now
    self.save
    self.issues
  end

  def issues_by_filter(filter = {})
    filter ||= {}
    return self.issues if filter[:labels].blank?
    self.issues.select{|issue| (filter[:labels] - issue[:labels]).blank? }
  end

  def project_by_issue(issue)
    self.projects.detect{|p| p[:id] == issue[:project_id]}
  end

  def labels
    self.settings(:data).labels
  end

  def labels= labels
    self.settings(:data).labels = labels
  end

  def projects
    self.settings(:data).projects ||= []
    self.settings(:data).projects
  end

  def projects= projects
    self.settings(:data).projects = projects
  end

  def issues
    self.settings(:data).issues
  end

  def issues= issues
    self.settings(:data).issues = issues
  end

  def control_config
    self.settings(:data).control_config
  end

  def control_config= config
    self.settings(:data).control_config = config
  end

  def expired?(object)
    self.control_config ||= {} 
    self.control_config[ "#{object + '_updated_at'}".to_sym] ||= DateTime.now.last_year
    self.control_config[ "#{object + '_updated_at'}".to_sym] + self.timeout < DateTime.now
  end

  def per_page
    100
  end

  def parse_project_attributes(project)
    {
      :id => project['id'], 
      :name => project['name'], 
#      :web_url => project['web_url'], 
      :active => false,
      :path_with_namespace => project['path_with_namespace'],
      :host => self.current_host,
    }
  end

  def parse_issue_attributes(issue)
    {
      :id => issue['id'], 
      :iid => issue['iid'], 
      :project_id => issue['project_id'], 
      :title => issue['title'], 
      :state => issue['state'], 
      :created_at => issue['created_at'], 
      :updated_at => issue['updated_at'], 
      :labels => issue['labels'] || [], 
      :milestone => issue['milestone'], 
      :assignee => issue['assignee'], 
      :author => issue['author'] 
    }
  end

end
