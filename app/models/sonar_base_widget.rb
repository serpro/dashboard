class SonarBaseWidget < Widget

  include HTTParty

  after_initialize do
    self.class.base_uri self.class.set_uri
  end

  def self.permit
    super + [:include_systems_re]
  end

  def self.set_uri(host= nil)
    data = self.servers_config.detect{|c| (c.blank? ?  false : c['default']) == true} || {}
    host = data.has_key?('host') ? data['host'] : 'define an uri'
    self.base_uri(host)
  end

  def pretty_name(name)
    name.gsub(/#{prefix_expression}/,'')
  end

  def prefix_expression
    'DESDR.DE[0-9|ED|CT]+.'
  end

  # Example of return
  # {"GED Build"=>0.0, "AFD"=>0.0, "ExtraTomDanado"=>20.0}
  def systems_coverage
    load_data #if self.sonar_cache.nil? || self.expired?
    coverage_info = {}

    sonar_cache.map do |info|
      coverage_value = info['msr'].detect{|value| value['key'] == 'coverage' } unless info['msr'].nil?
      coverage_info[info['name']] = coverage_value.nil? ? 0 : coverage_value['val']
    end
    
    coverage_info.select!{|k| k.match(/#{self.include_systems_re}/)} unless self.include_systems_re.nil?
    coverage_info.sort_by{|k,v| -v}
  end

  def expired?
    return true if self.updated_at.nil?
    (self.updated_at > (DateTime.now - self.timeout))
  end

  def include_systems_re
    self.settings(:data).include_systems_re
  end

  def include_systems_re= re
    self.settings(:data).include_systems_re = re
  end

  protected

  def timeout
    2.hours
  end

  def load_data
      data = self.class.get("/resources?metrics=ncloc,coverage&format=json", :basic_auth => auth)
      self.sonar_cache = data.response.is_a?(Net::HTTPOK) ? data.parsed_response : []
  end

  def auth
    {:username => username, :password => password}
  end

  def sonar_cache
    self.settings(:data).sonar_cache
  end

  def sonar_cache= sonar_cache
    self.settings(:data).sonar_cache = sonar_cache
  end

  def self.servers_config
    widget_config['servers'] || []
  end

  def servers_config
    self.class.servers_config
  end


  def username
    return nil if widget_config.blank?
    widget_config['authentication']['username']
  end

  def password
    return nil if widget_config.blank?
    widget_config['authentication']['password']
  end

end
