class SonarSmileWidget < SonarBaseWidget

  def self.feeling(coverage)
    coverage = coverage/10
    if coverage >= 0 && coverage < 2
      'very_sad'
    elsif  coverage >= 2 && coverage < 4
      'sad'
    elsif  coverage >= 4 && coverage < 6
      'normal'
    elsif  coverage >= 6 && coverage < 8
      'happy'
    elsif  coverage >= 8 && coverage <= 10
      'very_happy'
    end
  end

  def self.face(coverage)
    coverage = coverage/10
    if coverage >= 0 && coverage < 2
      ':('
    elsif  coverage >= 2 && coverage < 4
      ':/'
    elsif  coverage >= 4 && coverage < 6
      ':*'
    elsif  coverage >= 6 && coverage < 8
      ':)'
    elsif  coverage >= 8 && coverage <= 10
      ':D'
    end
  end
end
