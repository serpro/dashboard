class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :ldap_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  #FIXME make this test
  has_and_belongs_to_many :panels

  validates_presence_of :login

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "120x120>" }, default_url: ActionController::Base.helpers.asset_path("images/:style/default-user.png")
  validates_attachment :avatar, presence: true, content_type: { content_type: "image/jpeg" }

  #FIXME make this test
  before_create :get_ldap_parameters

  #FIXME make this test
  def get_ldap_parameters
    self.login = Devise::LDAP::Adapter.get_ldap_param(self.email,"uid").first
    self.avatar = base64_2_fileimage(Devise::LDAP::Adapter.get_ldap_param(self.email,"jpegPhoto").first)
  end

  #FIXME make this test
  def base64_2_fileimage(base64_data)
    random_string = SecureRandom.hex
    filename = "avatar-#{random_string}.jpg"
    f = File.open(File.join('tmp', filename), 'w+')
    f.write(base64_data.force_encoding('UTF-8'))
    f.close
    f = File.open(File.join('tmp', filename))
    f
  end

end
