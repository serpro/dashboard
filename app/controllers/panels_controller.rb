class PanelsController < ApplicationController

  before_filter :allow_iframe
  before_action :set_panel, only: [:show, :edit, :update, :destroy, :dashboard]

  def index
    @panels = Panel.visible_to_me(current_user).send(params[:panel_state] || 'active').paginate(:page => params[:page])
  end

  def my
    @panels = Panel.send(params[:panel_state] || 'active').paginate(:page => params[:page])
    render :action => :index
  end

  def show
    redirect_to panel_widgets_path(@panel)
  end

  def dashboard
    page = params[:page].to_i
    page += 1
    page = 1 if page > Panel.find(params[:id]).widgets.active.count
    @widget = Panel.find(params[:id]).widgets.active.paginate(:per_page => 1, :page => page).first
    params[:page] = page
  end

  # GET /panels/new
  def new
    @panel = Panel.new
  end

  # GET /panels/1/edit
  def edit
  end

  # POST /panels
  # POST /panels.json
  def create
    @panel = Panel.new(panel_params)
#current_user.valid?
#raise current_user.errors.inspect
    @panel.users << current_user if current_user

    if @panel.save
      redirect_to panel_widgets_path(@panel), notice: t(:panel_successfully_created)
    else
      render :new 
    end
  end

  # PATCH/PUT /panels/1
  # PATCH/PUT /panels/1.json
  def update
    respond_to do |format|
      if @panel.update(panel_params)
        format.html { redirect_to @panel, notice: 'Panel was successfully updated.' }
        format.json { render :show, status: :ok, location: @panel }
      else
        format.html { render :edit }
        format.json { render json: @panel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /panels/1
  # DELETE /panels/1.json
  def destroy
    @panel.destroy
    respond_to do |format|
      format.html { redirect_to panels_url, notice: 'Panel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_panel
      @panel = Panel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def panel_params
      params.require(:panel).permit(:name, :active, :visibility)
    end

    def allow_iframe
      response.headers.delete 'X-Frame-Options'
    end
end
