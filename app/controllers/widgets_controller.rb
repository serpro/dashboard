class WidgetsController < ApplicationController
  before_action :set_panel
  before_action :set_widget, only: [:show, :edit, :update, :destroy, :reload_data, :switch_activation, :dashboard]
  before_action :set_filter

  # GET /widgets
  # GET /widgets.json
  def index
    @widgets = @panel.widgets.paginate(:page => params[:page])
  end

  # GET /widgets/1
  # GET /widgets/1.json
  def show
  end

  def dashboard
  end

  # GET /widgets/new
  def new
    @widget = load_widget
  end

  # GET /widgets/1/edit
  def edit
  end

  # POST /widgets
  # POST /widgets.json
  def create
    @widget = load_widget#(widget_params)
    @widget.panel = @panel

    respond_to do |format|
      if @widget.save
        format.html { redirect_to panel_widget_path(@panel, @widget), notice: 'Widget was successfully created.' }
        format.json { render :show, status: :created, location: @widget }
      else
        format.html { render :new }
        format.json { render json: @widget.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if @widget.update(widget_params)
      redirect_to panel_widgets_path(@panel), notice: t(:widget_successfully_updated)
    else
      render :edit
    end
  end

  # DELETE /widgets/1
  # DELETE /widgets/1.json
  def destroy
    @widget.destroy
    respond_to do |format|
      format.html { redirect_to panel_widgets_url(@panel), notice: 'Widget was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def widget_partial
    @widget = load_widget
    respond_to do |format|
      format.js
    end
  end

  def reload_data
    @widget.reload_data!
    respond_to do |format|
      format.html { redirect_to panel_widget_path(@panel, @widget), notice: 'Widget was successfully reloaded.' }
      format.json { render :show, status: :ok, location: @widget }
    end
  end

  def switch_activation
    @widget.switch_activation!
    respond_to do |format|
      format.html { redirect_to panel_widgets_path(@panel), notice: 'Widget was successfully updated' }
#      format.json { render :show, status: :ok, location: @widget }
    end
  end
  private

    # Use callbacks to share common setup or constraints between actions.
    def set_panel
      @panel = Panel.find(params[:panel_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_widget
      @widget = @panel.widgets.find(params[:id])
    end
    def set_filter
      @filter ||= {}
      @filter[:labels] = (params[:labels] || '').split(',')
    end

    def load_widget
      klass = Widget.descendants.detect{|w| w.name == params[:type]}
      @widget = (klass || UrlWidget).new(widget_params(klass))
      @widget
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def widget_params(klass = nil)
      return {} if params[:widget].blank?
      params.require(:widget).permit((@widget.nil? ? klass : @widget.class).permit)
    end
end
