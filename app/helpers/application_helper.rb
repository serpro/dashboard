module ApplicationHelper

  def partial_for_widget(widget)
    widget.class.to_s.underscore
  end

  def widget_fullname(widget)
    t('widget_fullname', widget_name: widget.name, widget_type: widget.class.model_name.human)
  end

  def visibility_name(visibility)
    {
      Panel::Visibility::PRIVATE => t(:private),
      Panel::Visibility::PUBLIC_TO_VIEW => t(:public_to_view),
      Panel::Visibility::PUBLIC_TO_EDIT => t(:public_to_edit),
    }[visibility]
  end

end
