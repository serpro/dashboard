module WidgetsHelper
  
  def gitlab_issue_url(widget, issue)
    project = widget.project_by_issue(issue)
    url = project[:host] + '/' + project[:path_with_namespace] + "/issues/#{issue[:iid]}"
    label = GitlabWidget.server_name(project[:host]) + ': ' + project[:name] + ' - ' + issue[:iid].to_s + ': ' + issue[:title].to_s
    link_to(label, url, :target => "_blank")
  end

  def switch_activave_label(active)
    active ? t(:deactivate)  : t(:activate)
  end
 
end
