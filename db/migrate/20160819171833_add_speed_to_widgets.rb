class AddSpeedToWidgets < ActiveRecord::Migration
  def change
    add_column :widgets, :speed, :integer, :default => 3000
    Widget.update_all(speed: 3000)
  end
end
