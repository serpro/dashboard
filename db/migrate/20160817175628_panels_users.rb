class PanelsUsers < ActiveRecord::Migration
  def change
    create_table :panels_users do |t|
      t.references :panel
      t.references :user

      t.timestamps null: false
    end
  
  end
end
