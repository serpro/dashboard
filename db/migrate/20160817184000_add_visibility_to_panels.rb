class AddVisibilityToPanels < ActiveRecord::Migration
  def change
    add_column :panels, :visibility, :integer, :default => Panel::Visibility::PUBLIC_TO_VIEW
    Panel.update_all(visibility: Panel::Visibility::PUBLIC_TO_EDIT)
  end
end
