class AddDefaultValueForActiveWidgetColumn < ActiveRecord::Migration
  def change
    change_column_default(:widgets, :active, true)
    Widget.update_all(active: true)
  end
end
