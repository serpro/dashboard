class CreateWidgets < ActiveRecord::Migration
  def change
    create_table :widgets do |t|
      t.string :name, :type
      t.boolean :active
      t.belongs_to :panel

      t.timestamps null: false
    end
  end
end
