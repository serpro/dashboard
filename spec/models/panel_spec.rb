require 'rails_helper'

describe Panel do

  describe '#save' do

    context 'with invalid attribute' do
      it '#name not filled' do 
        expect(build(:panel, name: nil)).to_not be_valid
      end

    end

    context 'with valid attribute' do
      it '#name is filled' do 
        expect(build(:panel)).to be_valid
      end
    end

  end

end
