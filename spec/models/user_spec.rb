require 'rails_helper'

RSpec.describe User, type: :model do

  describe '#save' do

    context 'with invalid attribute' do
      it '#login not filled' do 
        expect(build(:user, login: nil)).to_not be_valid
      end

    end

    context 'with valid attribute' do
      it '#login is filled' do 
        expect(build(:user, login: 'some name')).to be_valid
      end
    end

    context 'with default attributes' do
      it '#admin is false' do 
        expect(build(:user).admin).to be_falsey
      end
      it '#active is true' do 
        expect(build(:user).active).to be_truthy
      end
    end
  end

end
