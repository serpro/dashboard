FactoryGirl.define do
  factory :user do
    login { FFaker::Lorem.words.to_s }
    password { 123456 }
    email { FFaker::Internet.email}
  end

end
