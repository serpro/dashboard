require 'ffaker'
FactoryGirl.define do
  factory :panel do
    name { FFaker::Lorem.words.to_s }
    active true
  end

  factory :widget do
    name { FFaker::Lorem.words.to_s }
    active true
    panel   
  end

  trait :active do
    active true
  end

  trait :inactive do
    active false
  end

end
