require 'rails_helper'

feature 'User manipulate panel list' do
  before do
    Panel.delete_all
  end

  scenario 'could go to any paginated page' do
    create_list(:panel, 7)
    panel = Panel.last
    visit panels_path

    click_link '2'

    expect(page).to have_css "#panel-#{panel.id}"
  end

  scenario 'do not have inactive by default' do
    create_list(:panel, 2)
    create_list(:panel, 1, :inactive)
    first_panel = Panel.first
    last_panel = Panel.last
    visit panels_path
    expect(page).to have_css("a", :text => first_panel.name)
    expect(page).not_to have_css("a", :text => last_panel.name)
  end

  scenario 'select only inactive panels', :js => true do
    create_list(:panel, 2)
    create_list(:panel, 1, :inactive)
    panel = Panel.last
    visit panels_path

    select('Inactive', :from => 'panel_state')

    expect(page).to have_css "#panel-#{panel.id}"
  end
end
