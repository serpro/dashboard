require 'rails_helper'

RSpec.describe PanelsController, type: :controller do

  describe 'GET #index' do
    before do 
      Panel.delete_all
    end

    it "loads the panels into @panels" do
      p1, p2 = create(:panel), create(:panel)
      get :index

      expect(assigns(:panels)).to match_array([p1, p2])
    end

    it 'load only five panels per page' do
      create_list(:panel, 6)
      get :index

      expect(assigns(:panels).length).to eq(5)
    end

    it 'load only active panels by default' do
      create_list(:panel, 2, :active)
      create_list(:panel, 3, :inactive)
      get :index

      expect(assigns(:panels).length).to eq(2)
    end

  end 

  describe 'POST #create' do
    before do 
      Panel.delete_all
    end

    context "with valid attributes" do

      it 'creates the panel' do
        get :create, panel: attributes_for(:panel)
  
        expect(Panel.count).to eq(1)
      end

#      it 'redirects to the "show" action for the new panel' do
#        post :create, panel: attributes_for(:panel)
#        expect(response).to redirect_to Panel.first
#      end

    end

    context 'with invalid attributes' do

#      it 'does not create the panel' do
#        post :create, panel: attributes_for(:panel, name: nil)
#        expect(Panel.count).to eq(0)
#      end
#
#      it 're-renders the "new" view' do
#        post :create, panel: attributes_for(:panel, year: nil)
#        expect(response).to render_template :new
#      end

    end

  end


end
