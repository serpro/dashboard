require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class LocalOverride < Authenticatable
      def valid?
        true
      end

      def authenticate!
        if params[:user]
          user = User.where(email: params[:user][:email]).first

          if user && user.valid_password?(params[:user][:password])
            # Check if the user exists, is able to log in and the password is valid.
            success!(user)
          else
            fail
          end
        else
          fail
        end
      end
    end
  end
end

Warden::Strategies.add(:local_override, Devise::Strategies::LocalOverride)
