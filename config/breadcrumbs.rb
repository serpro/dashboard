crumb :root do
  link t(:breadcrumb_home).html_safe, root_path
end

crumb :panels do
  link t(:panels), panels_path
end

crumb :my_panels do
  link t(:my_panels), panels_path
end

crumb :new_panel do
  link t(:new_panel), new_panel_path
end

crumb :edit_panel do
  link t(:edit_panel), edit_panel_path
end

crumb :panel do |panel|
  link panel.name, panel_path(panel)
  parent :root
end

crumb :list_widgets do |panel|
  link t(:widgets), panel_widgets_path(panel)
  parent :panel, panel
end

crumb :new_widget do |panel|
  link t(:new_widget), panel_widgets_path(panel)
	  parent :list_widgets, panel
end

crumb :edit_widget do |panel, widget|
  link widget.name, edit_panel_widget_path(panel, widget)
  parent :list_widgets, panel
end

crumb :show_widget do |panel, widget|
  link widget.name, panel_widget_path(panel, widget)
  parent :list_widgets, panel
end
# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).
